package threads;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Exemplo de execu��o paralela de threads
 * @author Hallison
 *
 */
public class View extends JFrame{

	private JPanel contentPane;
	private JTextField txtThread1;
	private JTextField txtThread2;
	
	private JButton btnStartT1;
	private JButton btnStartT2;
	private JButton btnPausaT1;
	private JButton btnPausaT2;
	
	private CountThread1 thread1;
	private CountThread2 thread2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					View frame = new View();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public View() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 205);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblThread1 = new JLabel("Thread 1");
		lblThread1.setHorizontalAlignment(SwingConstants.CENTER);
		lblThread1.setBounds(10, 11, 197, 33);
		contentPane.add(lblThread1);
		
		JLabel lblThread2 = new JLabel("Thread 2");
		lblThread2.setHorizontalAlignment(SwingConstants.CENTER);
		lblThread2.setBounds(227, 11, 197, 33);
		contentPane.add(lblThread2);
		
		txtThread1 = new JTextField();
		txtThread1.setHorizontalAlignment(SwingConstants.CENTER);
		txtThread1.setBounds(10, 55, 197, 20);
		contentPane.add(txtThread1);
		txtThread1.setColumns(10);
		
		txtThread2 = new JTextField();
		txtThread2.setHorizontalAlignment(SwingConstants.CENTER);
		txtThread2.setBounds(227, 55, 197, 20);
		contentPane.add(txtThread2);
		txtThread2.setColumns(10);
		
		btnStartT1 = new JButton("Start T1");
		btnStartT1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnStartT1.setEnabled(false);
				btnPausaT1.setEnabled(true);
				thread1 = new CountThread1();
				thread1.start();
			}
		});
		btnStartT1.setBounds(10, 88, 197, 23);
		contentPane.add(btnStartT1);
		
		
		btnPausaT1 = new JButton("Pausa T1");
		btnPausaT1.setEnabled(false);
		btnPausaT1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnStartT1.setEnabled(true);
				if (thread1.isAlive()) {
					thread1.interrupt();
					System.out.println("Thread " + thread1.getName() + " foi interrompida!");
				} else if (thread1.isInterrupted()) {
					thread1.start();
				}
			}
		});
		btnPausaT1.setBounds(10, 122, 197, 23);
		contentPane.add(btnPausaT1);
		
		btnPausaT2 = new JButton("Pausa T2");
		btnPausaT2.setEnabled(false);
		btnPausaT2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnStartT2.setEnabled(true);
				if (thread2.isAlive()) {
					thread2.interrupt();
					System.out.println("Thread " + thread2.getName() + " foi interrompida!");
				} else if (thread2.isInterrupted()) {
					thread2.start();
				}
				
			}
		});
		btnPausaT2.setBounds(227, 122, 197, 23);
		contentPane.add(btnPausaT2);
		
		btnStartT2 = new JButton("Start T2");
		btnStartT2.setBounds(227, 86, 197, 23);
		btnStartT2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				btnStartT2.setEnabled(false);
				btnPausaT2.setEnabled(true);
				thread2 = new CountThread2();
				thread2.start();
			}
		});
		
		contentPane.add(btnStartT2);
	}
	
	/**
	 * Implementa��o da Thread 1
	 * @author Hallison
	 *
	 */
	private class CountThread1 extends Thread{
		
		@Override
		public void run() {
			
			for (int i = 0; i < 10000; i++) {
				txtThread1.setText(String.valueOf(i));
				System.out.println("Thread " + getName() + " - N�mero: " + String.valueOf(i));
				
				if (!isInterrupted()) {
					try {
						sleep(1000);
					} catch (InterruptedException ie) {
						System.out.println("Erro ao dormir a thread " + getName() + ".\n" + ie.getMessage());
					}
				}
			}
		}
		
	}
	
	/**
	 * Implementa��o da Thread 2
	 * @author Hallison
	 *
	 */
	private class CountThread2 extends Thread{
		
		@Override
		public void run() {
			
			for (int i = 0; i < 10000; i++) {
				txtThread2.setText(String.valueOf(i));
				System.out.println("Thread " + getName() + " - N�mero: " + String.valueOf(i));
				
				if (!isInterrupted()) {
					try {
						sleep(1000);
					} catch (InterruptedException ie) {
						System.out.println("Erro ao dormir a thread " + getName() + ".\n" + ie.getMessage());
					}
				}
			}
		}
		
	}
}
