package propertiesRead;

/**
 * Entidade SystemProperty
 * @author Hallison
 *
 */
public class SystemProperty {
	
	private String hostname;
	private String ipAddress;
	private String netmask;
	
	public SystemProperty(){
	
	}
	
	public SystemProperty (String hostname, String ipAddress, String netmask){
		super();
		this.hostname = hostname;
		this.ipAddress = ipAddress;
		this.netmask = netmask;
	}
	
	
	/**
	 * M�trodo que transforma o objeto em string
	 * @return string
	 */
	public String toString() {
		return "System [hostname=" + hostname + ", ipAddress=" + ipAddress + ", netmask=" + netmask + "]";
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getNetmask() {
		return netmask;
	}

	public void setNetmask(String netmask) {
		this.netmask = netmask;
	}
	
}
