package propertiesRead;

import java.util.ResourceBundle;


/**
 * Classe de exemplo para leitura de um arquivo de propriedades
 * @author Hallison
 *
 */
public class ReadFileProperties {
	
	/**
	 * Classe que l� o arquivo de propriedades "system.properties" no pacote "propertiesRead"
	 * @return system
	 */
	private static SystemProperty getProperties(){
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle("propertiesRead.system");
		
		String hostname = new String(resourceBundle.getString("hostname"));
		String ipAddress = new String(resourceBundle.getString("ipAddress"));
		String netmask = new String(resourceBundle.getString("netmask"));
		
		SystemProperty system = new SystemProperty(hostname, ipAddress, netmask);
		
		return system;		
	}
	
	
	/**
	 * Class main
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Iniciando...");
		
		SystemProperty systemProperty = getProperties();
		
		System.out.println(systemProperty.toString());
		
		System.out.println("Finalizado!");
		
	}

}
